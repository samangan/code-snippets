//Referencing the code review session on Thursday August 1, 2013

val tokens = List("i", "a", "b" ,"cat", "dog", "a", "dog", "dog", "never", "look", "I", "i", "test")



tokens.count(_ == "b") //this would return the number of times b is in the list (1)

def countListOccurences(list: List[String]): Map[String, Int] = {
  var seen: Map [String, Int] = Map()
  list.foreach((i: String) => seen += (i -> list.count(_ == i)))
  seen
}

countListOccurences(tokens)



//A more compact version (no need to make the map a var)
//I know this can be a lot more compact still.
//This can probably be all done in one function call.
def countListOccurencesCompact(list: List[String]): Map [String, Int] = {
  list.map((i: String) => i -> list.count(_ == i)).toMap
}
countListOccurencesCompact(tokens)

//Note: the second function is more compact because foreach is like map

// but foreach does not return anything and Map returns a list
// Whenever I make a variable and incrementally add to it I ask myself:
//  "Can I get all of this at once instead?"

//used as stubs for Roger's real functions
def _notStopWord(str: String): Boolean = if(str.equals("dog")) false else true
def _validWord(str: String): Boolean = if(str.equals("i")) false else true

def countListOccurencesRedux(list: List[String]): Map[String, Int] = {
  list.filter((i: String) => _notStopWord(i) && _validWord(i))
  .map((i: String) => i -> list.count(_ == i))
  .toMap
}

countListOccurencesRedux(tokens)



//example of how to deal with the debug logging
val verbose = true
if(verbose) {
  val (added, removed) = tokens.partition((i: String) => _notStopWord(i) && _validWord(i))
  added.foreach((i) => println("addedToken " + i))
  removed.foreach((i) => println("removedToken " + i))
}

